package com.evenmo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.evenmo"})
@MapperScan(basePackages = "com.evenmo.mapper")
public class SpringBootApplicationRun {
	public static void main(String[] args) {

		ConfigurableApplicationContext app = SpringApplication.run(SpringBootApplicationRun.class, args);


	}


}
