package com.evenmo.mapper;


import com.evenmo.pojo.Clazz;

/**
 * This is Description
 *
 * @author moyongjun
 * @date 2019/12/05
 */

public interface ClazzMapper {

    Clazz queryById(Integer id);


}
