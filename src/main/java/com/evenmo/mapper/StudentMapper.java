package com.evenmo.mapper;


import com.evenmo.pojo.Student;

/**
 * This is Description
 *
 * @author moyongjun
 * @date 2019/12/05
 */

public interface StudentMapper {

    Student queryById(Integer id);

    Student queryHaveClazzById(Integer id);
}
