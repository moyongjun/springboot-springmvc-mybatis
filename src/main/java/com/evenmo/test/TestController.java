package com.evenmo.test;

import com.evenmo.SpringBootApplicationRun;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * This is Description 测试Controller
 *
 * @author moyongjun
 * @date 2019/12/05
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApplicationRun.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)//配置一个随机端口
public class TestController {

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * @LocalServerPort 提供了 @Value("${local.server.port}") 的代替
     */
    @LocalServerPort
    private Integer port;

    private URL base;

    @Before
    public void initUrlAndPort(){

        String url = String.format("http://localhost:%d/", port);

        System.out.println(String.format("port is : [%d]", port));
        System.out.println("url is :"+url);
        try {
            this.base = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testqueryHaveClazzById(){

//        Map<String,Object> param=new HashMap<>();
//        param.put("id",1);
        ResponseEntity<String> forEntity =

                restTemplate.getForEntity(base.toString()+"/student/studentInfo/1", String.class);


        String body = forEntity.getBody();

        System.out.println(body);
    }
}
