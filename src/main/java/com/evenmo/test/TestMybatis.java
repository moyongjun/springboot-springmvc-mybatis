package com.evenmo.test;

import com.evenmo.SpringBootApplicationRun;
import com.evenmo.mapper.StudentMapper;
import com.evenmo.pojo.Student;
import com.evenmo.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * This is Description
 *
 * @author moyongjun
 * @date 2019/12/05
 */
@RunWith(SpringRunner.class)

//@SpringBootTest(classes = { SpringBootApplicationRun.class, StudentService.class },
//        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)//配置启动类，测试Contrller时用

//光写这个也可以，但要结合RunWith一起使用才有效
@SpringBootTest
public class TestMybatis {

    @Autowired
    StudentService studentService;

    @Autowired
    StudentMapper studentMapper;

    @Test
    public void test(){

        Student student = studentService.queryHaveClazzById(1);
        System.out.println(student);
    }


    @Test//必须用junit的包
    public void test2(){

        Student student = studentMapper.queryHaveClazzById(1);
        System.out.println(student);
    }


}
