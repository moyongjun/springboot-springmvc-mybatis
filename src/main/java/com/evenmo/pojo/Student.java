package com.evenmo.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * This is Description
 *
 * @author moyongjun
 * @date 2019/12/05
 */
@Data
public class Student implements Serializable {

    private Integer id;
    private String name;
    private Integer age;
    private String gender;
    private Integer cid;
    private Clazz clazz;//关联查询班级信息，用自动匹配方式，即resultType
}
