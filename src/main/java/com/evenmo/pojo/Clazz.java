package com.evenmo.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * This is Description
 *
 * @author moyongjun
 * @date 2019/12/05
 */
@Data
public class Clazz implements Serializable {

    private Integer id;
    private String name;
    private String room;
}
