package com.evenmo.service;

import java.util.List;

import com.evenmo.pojo.Student;

public interface StudentService {

	public Student queryStudentInformationsByStudentId(Student student);
	public Student queryHaveClazzById(Integer id);

}
