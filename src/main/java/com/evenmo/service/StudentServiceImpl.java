package com.evenmo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.evenmo.mapper.StudentMapper;
import com.evenmo.pojo.Student;
import com.github.pagehelper.PageHelper;

@Service
public class StudentServiceImpl implements StudentService {
	@Autowired
	private StudentMapper studentDao;

	@Override
	public Student queryStudentInformationsByStudentId(Student student) {
		// TODO Auto-generated method stub
		return studentDao.queryById(1);
	}

	@Override
	public Student queryHaveClazzById(Integer id) {
		return studentDao.queryHaveClazzById(id);
	}


}
