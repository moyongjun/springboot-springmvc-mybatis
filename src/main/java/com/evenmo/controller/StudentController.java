package com.evenmo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.evenmo.pojo.Student;
import com.evenmo.service.StudentService;
@RequestMapping(value="/student")
@Controller
public class StudentController {
@Autowired
private StudentService studentService;
@RequestMapping(value="/studentInfo/{id}")
@ResponseBody
public Student showStudentById(@PathVariable(value="id")Integer id) {
	Student s=new Student();
	s.setId(id);
	Student stu = studentService.queryStudentInformationsByStudentId(s);
	return stu;
}


	@RequestMapping(value="/studentInfo/classinfo/{id}")
	@ResponseBody
	public Student queryHaveClazzById(@PathVariable(value="id")Integer id) {

		Student stu = studentService.queryHaveClazzById(id);
		return stu;
	}


}
